import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Particles 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: particleSystemWrapper

    Image {
        id: background
        anchors.centerIn: parent
        width: background.sourceSize.width
        height: background.sourceSize.height
        //fillMode: Image.PreserveAspectCrop
        visible: true
        source: "fullsize_background.png"
    }

    Timer {
        running: true
        repeat: true
        interval: 10
        onTriggered: shader.time += 0.05
    }

    ShaderEffect {
        id: shader
        width: background.sourceSize.width
        height: background.sourceSize.height
        visible: false
        blending: true

        property variant src: background
        property variant resolution: Qt.vector2d(background.width, parent.height)
        property variant g_NitroSpeeds: Qt.vector4d(-0.1, 0.7, 0.1, 0)
        property variant g_NitroScales: Qt.vector2d(1, 2)
        property real time: 0
        vertexShader: window.loadFromFile("effect.vert")
        fragmentShader: window.loadFromFile("effect.frag")
    }

    Image {
        id: mask
        visible: false
        source: "mask_transparent.png"
        fillMode: Image.PreserveAspectCrop
        sourceSize: Qt.size(background.sourceSize.width, background.sourceSize.height)
    }

    ThresholdMask {
        id: thresholdMask
        width: background.sourceSize.width
        height: background.sourceSize.height
        source: shader
        maskSource: mask
        threshold: 0.9
        spread: 0.3
        visible: false
    }

    Blend {
        anchors.fill: background
        source: background
        foregroundSource: thresholdMask
        mode: "addition"
    }

    MouseArea {
        id: ma
        anchors.fill: parent
        hoverEnabled: true
        onPressed: {
            attractor.enabled = true
        }
        onPositionChanged: {
            if (ma.pressed) {
                attractor.pointX = mouseX
                attractor.pointY = mouseY
            }
        }
        onReleased: {
            attractor.enabled = false
        }
    }

    Attractor {
        id: attractor
        system: particleSystem
        affectedParameter: Attractor.Acceleration
        strength: 8000000
        proportionalToDistance: Attractor.InverseQuadratic
    }

    ParticleSystem {
        id: particleSystem
    }

    Emitter {
        id: emitter
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }

        width: parent.width
        height: parent.height
        system: particleSystem
        emitRate: 50
        lifeSpan: 5000
        lifeSpanVariation: 1000
        size: 8
        endSize: 18
        sizeVariation: 4
        velocity: AngleDirection {
            angle: 90
            magnitude: 50
            magnitudeVariation: 25
            angleVariation: 10
        }
    }

    ImageParticle {
        height: 16
        width: 16
        color: "white"
        source: "dot.png"
        system: particleSystem
        opacity: .75
    }
}
